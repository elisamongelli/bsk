package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {
	
	@Test
	public void testGetOneFrameOfTheGame() throws BowlingException {
		int[] expectedFrame = {7,2};
		int[] actualFrame = new int[2];
		
		Game game = new Game();
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		actualFrame[0] = game.getFrameAt(2).getFirstThrow();
		actualFrame[1] = game.getFrameAt(2).getSecondThrow();
		
		assertArrayEquals(expectedFrame, actualFrame);
	}
	
	@Test (expected = BowlingException.class)
	public void testShouldThrowExceptionNumberOfFrames() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		game.addFrame(new Frame(2,6));
		game.addFrame(new Frame(2,6));
	}
	
	@Test (expected = BowlingException.class)
	public void testShouldThrowExceptionNegativeIndex() throws BowlingException {
		int[] actualFrame = new int[2];
		
		Game game = new Game();
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		actualFrame[0] = game.getFrameAt(-1).getFirstThrow();
	}

	@Test (expected = BowlingException.class)
	public void testShouldThrowExceptionIndexOutOfRange() throws BowlingException {
		int[] actualFrame = new int[2];
		
		Game game = new Game();
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		actualFrame[0] = game.getFrameAt(11).getFirstThrow();
	}
	
	@Test
	public void testGetGameScore() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWithLessThanTenFrames() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		
		assertEquals(55, game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWithSpareFrame() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,3));	// spare frame
		game.addFrame(new Frame(4,2));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(83, game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWithStrikeFrame() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(10,0));		// strike frame
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWithStrikeFrameFollowedBySpareFrame() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(10,0));		// strike frame
		game.addFrame(new Frame(4,6));		// spare frame
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWithStrikeFrameFollowedByStrikeFrame() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(10,0));		// first strike frame
		game.addFrame(new Frame(10,0));		// second strike frame
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWithSpareFrameFollowedBySpareFrame() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(8,2));		// first spare frame
		game.addFrame(new Frame(5,5));		// second spare frame
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWithLastFrameSpare() throws BowlingException {
		int bonusThrow = 7;
		Game game = new Game();
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));	// spare on last frame
		
		game.setFirstBonusThrow(bonusThrow);
		
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWithLastFrameStrike() throws BowlingException {
		int firstBonusThrow = 7;
		int secondBonusThrow = 2;
		Game game = new Game();
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));		// strike on last frame
		
		game.setFirstBonusThrow(firstBonusThrow);
		game.setSecondBonusThrow(secondBonusThrow);
		
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testGetScoreOfPerfectGame() throws BowlingException {
		int firstBonusThrow = 10;
		int secondBonusThrow = 10;
		Game game = new Game();
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		
		game.setFirstBonusThrow(firstBonusThrow);
		game.setSecondBonusThrow(secondBonusThrow);
		
		assertEquals(300, game.calculateScore());
	}
	
	@Test
	public void testGetScoreWithNinthStrikeFrameAtTenthGeneralFrame() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,3));
		
		assertEquals(255, game.calculateScore());
	}
	
	@Test
	public void testGetScoreWithNinthStrikeFrameAtTenthSpareFrame() throws BowlingException {
		int firstBonusThrow = 5;
		Game game = new Game();
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,3));
		
		game.setFirstBonusThrow(firstBonusThrow);
		
		assertEquals(272, game.calculateScore());
	}
}
