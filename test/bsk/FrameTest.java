package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {
	
	@Test
	public void testGetFirstThrow() throws Exception{
		int firstThrow = 2;
		Frame frame = new Frame(firstThrow, 4);
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	public void testGetSecondThrow() throws Exception{
		int secondThrow = 4;
		Frame frame = new Frame(2, secondThrow);
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test (expected = BowlingException.class)
	public void testShouldReturnExceptionNegativeFirstThrow() throws Exception{
		int firstThrow = -1;
		Frame frame = new Frame(firstThrow, 4);
	}
	
	@Test (expected = BowlingException.class)
	public void testShouldReturnExceptionFirstThrowGreaterThanTen() throws Exception{
		int firstThrow = 11;
		Frame frame = new Frame(firstThrow, 4);
	}
	
	@Test (expected = BowlingException.class)
	public void testShouldReturnExceptionNegativeSecondThrow() throws Exception{
		int secondThrow = -1;
		Frame frame = new Frame(2, secondThrow);
	}

	@Test (expected = BowlingException.class)
	public void testShouldReturnExceptionSecondThrowGreaterThanTen() throws Exception{
		int secondThrow = 11;
		Frame frame = new Frame(2, secondThrow);
	}
	
	@Test
	public void testGetFrameScore() throws Exception{
		Frame frame = new Frame(2, 6);
		assertEquals(8, frame.getScore());
	}
	
	@Test
	public void testCheckIsSpare() throws Exception{
		Frame frame = new Frame(1,9);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testCheckIsNotSpare() throws Exception{
		Frame frame = new Frame(1,8);
		assertFalse(frame.isSpare());
	}
	
	@Test
	public void testGetSpareFrameScore() throws Exception{
		Frame frame1 = new Frame(1,9);
		Frame frame2 = new Frame(3,6);
		
		frame1.setBonus(frame2.getFirstThrow());
		assertEquals(13, frame1.getScore());
	}
	
	@Test
	public void testCheckIsStrike() throws Exception{
		Frame frame = new Frame(10,0);
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void testCheckIsNotStrike() throws Exception{
		Frame frame = new Frame(9,0);
		assertFalse(frame.isStrike());
	}
	
	@Test
	public void testGetStrikeFrameScore() throws Exception{
		Frame frame1 = new Frame(10,0);
		Frame frame2 = new Frame(3,6);
		
		frame1.setBonus(frame2.getFirstThrow() + frame2.getSecondThrow());
		assertEquals(19, frame1.getScore());
	}
}
