package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	
	ArrayList<Frame> frames;
	public static final int NUMBER_OF_FRAMES = 10;
	private int firstBonusThrow;
	private int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.frames = new ArrayList<Frame>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		if(frames.size() >= 10)
			throw new BowlingException("Can't add any more frames");
		
		frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		
		if(index < 0 || index > 9)
			throw new BowlingException("Frame index out of range");
		
		return frames.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int totalScore = 0;
		
		for(int i = 0; i < NUMBER_OF_FRAMES; i++) {
			
			if (frames.size() < 10 && i >= frames.size()) {
				totalScore += 0;	// considers missing frame scores as 0, if less than 10 frames have been added to the game
				continue;
			}
			
			if (frames.get(i).isStrike())
				if (i == NUMBER_OF_FRAMES-1)
					frames.get(i).setBonus(firstBonusThrow + secondBonusThrow);
				else if (frames.get(i+1).isStrike())
					if (i == NUMBER_OF_FRAMES-2)
						frames.get(i).setBonus(frames.get(i+1).getFirstThrow() + firstBonusThrow);
					else frames.get(i).setBonus(frames.get(i+1).getFirstThrow() + frames.get(i+2).getFirstThrow());
				else frames.get(i).setBonus(frames.get(i+1).getFirstThrow() + frames.get(i+1).getSecondThrow());
			
			else if (frames.get(i).isSpare())
				if (i == NUMBER_OF_FRAMES-1)
					frames.get(i).setBonus(firstBonusThrow);
				else frames.get(i).setBonus(frames.get(i+1).getFirstThrow());
			
			totalScore += frames.get(i).getScore();
		}
		
		return totalScore;
	}

}
